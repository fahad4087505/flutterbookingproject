import 'dart:async';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:quiver/strings.dart';

import '../../../common/config.dart';
import '../../../common/constants/general.dart';
import '../../../common/tools.dart';
import '../../../models/index.dart';
import '../../../services/base_services.dart';
import '../../../services/wordpress/blognews_api.dart';
import '../mapping/mapping.dart';
import 'listing_api.dart';

class ListingService with EmptyServiceMixin implements BaseServices {
  static final ListingService _instance = ListingService._internal();

  factory ListingService() => _instance;

  ListingService._internal();

  String url;

  ListingAPI listingAPI;

  List<Category> cats;
  List<Map<String, dynamic>> product_options;
  List<Map<String, dynamic>> product_option_values;
  String id_lang;
  String language_code;

  @override
  BlogNewsApi blogApi;

  void appConfig(appConfig) {
    blogApi = BlogNewsApi(appConfig["blog"] ?? appConfig["url"]);
    listingAPI = ListingAPI(appConfig["url"], appConfig["consumerKey"],
        appConfig["consumerSecret"]);
    url = appConfig["url"];
    Mapping.init(appConfig["type"]);
  }

  @override
  Future<List<Order>> getMyOrders({UserModel userModel, int page}) async {
    try {
      var response = await listingAPI.getAsync(
          "orders?customer=${userModel.user.id}&per_page=20&page=$page");
      List<Order> list = [];
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Order.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> createUser({
    String firstName,
    String lastName,
    String username,
    String password,
    String phoneNumber,
    bool isVendor = false,
  }) async {
    try {
      String niceName = firstName + " " + lastName;
      final http.Response response = await http.post(
          "$url/wp-json/api/flutter_user/register/?insecure=cool&",
          body: convert.jsonEncode({
            "user_email": username,
            "user_login": username,
            "username": username,
            "user_pass": password,
            "email": username,
            "user_nicename": niceName,
            "display_name": niceName,
          }));
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body["message"] == null) {
        var cookie = body['cookie'];
        return await getUserInfo(cookie);
      } else {
        var message = body["message"];
        throw Exception(message != null ? message : "Can not create the user.");
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<BlogNews>> fetchBlogLayout({config, lang}) {
    // TODO: implement fetchBlogLayout
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> fetchProductsByCategory(
      {categoryId,
      tagId,
      page,
      minPrice,
      maxPrice,
      orderBy,
      lang,
      order,
      featured,
      onSale,
      attribute,
      attributeTerm}) async {
    try {
      List<Product> list = [];

      var endPoint =
          "$url/wp-json/wp/v2/${DataMapping().kProductPath}?_embed=true&per_page=10&page=$page";
      if (int.parse(categoryId) > -1 && categoryId != null) {
        endPoint += "&${DataMapping().kCategoryPath}=$categoryId";
      }
      if (orderBy != null) {
        endPoint += "&orderby=$orderBy";
      }
      if (order != null) {
        endPoint += "&order=$order";
      }
      var response = await http.get(endPoint);
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)) {
          Product product = Product.fromListingJson(item);
          list.add(product);
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> fetchProductsLayout({config, lang}) async {
    try {
      List<Product> list = [];

      var endPoint =
          "$url/wp-json/wp/v2/${DataMapping().kProductPath}?_embed=true&per_page=10";
      if (config.containsKey("category")) {
        endPoint += "&${DataMapping().kCategoryPath}=${config["category"]}";
      }

      var response = await http.get(endPoint);
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)) {
          Product product = Product.fromListingJson(item);
          list.add(product);
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Category>> getCategories({lang}) async {
    try {
      var endpoint =
          "$url/wp-json/wp/v2/${DataMapping().kCategoryPath}?hide_empty=true&_embed&per_page=100";
      var response = await http.get(endpoint);
      final body = convert.jsonDecode(response.body);
      if (body is Map && body["message"] != null) {
        throw Exception(body["message"]);
      } else {
        List<Category> list = [];
        for (var item in body) {
          list.add(Category.fromListingJson(item));
        }
        return list;
      }
    } catch (e) {
      printLog('getCategories: $e');
      rethrow;
    }
  }

  @override
  Future<BlogNews> getPageById(int pageId) {
    // TODO: implement getPageById
    throw UnimplementedError();
  }

  @override
  Future<List<PaymentMethod>> getPaymentMethods(
      {CartModel cartModel,
      ShippingMethod shippingMethod,
      String token}) async {
    try {
      var endpoint = '$url/wp-json/wp/v2/payment';

      if (token != null) {
        endpoint += '?cookie=$token';
      }
      List<PaymentMethod> list = [];
      final http.Response response = await http.get(
        endpoint,
      );
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        for (var item in body) {
          list.add(PaymentMethod.fromJson(item));
        }
      }
      if (list.isEmpty) {
        throw Exception("No payment methods");
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future updateOrder(orderId, {status, token}) async {
    try {
      var response = await listingAPI
          .postAsync("orders/$orderId", {"status": status}, version: 2);
      if (response["message"] != null) {
        throw Exception(response["message"]);
      } else {
        return Order.fromJson(response);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>> getProducts() {
    // TODO: implement getProducts
    throw UnimplementedError();
  }

  @override
  Future<List<ShippingMethod>> getShippingMethods(
      {CartModel cartModel, String token, String checkoutId}) {
    // TODO: implement getShippingMethods
    throw UnimplementedError();
  }

  @override
  Future<User> login({username, password}) async {
    var cookieLifeTime = 120960000000;
    try {
      final http.Response response = await http.post(
          "$url/wp-json/api/flutter_user/generate_auth_cookie",
          body: convert.jsonEncode({
            "seconds": cookieLifeTime.toString(),
            "username": username,
            "password": password
          }));

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200 && body['cookie'] != null) {
        return await getUserInfo(body['cookie']);
      } else {
        throw Exception("The username or password is incorrect.");
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      printLog(err);
      rethrow;
    }
  }

  @override
  Future<User> loginFacebook({String token}) async {
    const cookieLifeTime = 120960000000;

    try {
      var endPoint =
          "$url/wp-json/api/flutter_user/fb_connect/?second=$cookieLifeTime"
          "&access_token=$token";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['id'] == null || jsonDecode["cookie"] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromListingJson(jsonDecode);
    } catch (e) {
      printLog(e);
      rethrow;
    }
  }

  @override
  Future<User> loginSMS({String token}) async {
    try {
      var endPoint =
          "$url/wp-json/api/flutter_user/firebase_sms_login?phone=$token";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['id'] == null || jsonDecode["cookie"] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromListingJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginApple({String email, String fullName}) async {
    try {
      var endPoint =
          "$url/wp-json/api/flutter_user/apple_login?email=$email&display_name=$fullName&user_name=${email.split("@")[0]}";

      var response = await http.get(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['id'] == null || jsonDecode["cookie"] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromListingJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> getUserInfo(cookie) async {
    try {
      final http.Response response = await http.get(
          "$url/wp-json/api/flutter_user/get_currentuserinfo?cookie=$cookie");
      final body = convert.jsonDecode(response.body);
      if (body["message"] == null) {
        return User.fromListingJson(body);
      } else {
        throw Exception(body["message"]);
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginGoogle({String token}) async {
    const cookieLifeTime = 120960000000;

    try {
      var endPoint =
          "$url/wp-json/api/flutter_user/google_login/?second=$cookieLifeTime"
          "&access_token=$token";
      var response = await http.get(endPoint);
      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['id'] == null || jsonDecode["cookie"] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromListingJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      printLog(e);
      rethrow;
    }
  }

  @override
  Future<List<Review>> getReviews(productId) async {
    try {
      List<Review> list = [];

      ///get reviews for my listing/listeo
      if (DataMapping().kListingReviewMapping['review'] == 'getReviews') {
        final response = await http.get(
            '$url/wp-json/wp/v2/${DataMapping().kListingReviewMapping['review']}/$productId?per_page=100');
        if (response.statusCode == 200) {
          for (Map<String, dynamic> item in convert.jsonDecode(response.body)) {
            try {
              Review review = Review.fromListing(item);
              if (review.status == 'approved') {
                list.add(review);
              }
            } catch (e) {
              printLog('Error converting review Listing $e');
            }
          }
        }
        return list;
      }

      ///get reviews for listingpro
      final response = await http.get(
          '$url/wp-json/wp/v2/${DataMapping().kListingReviewMapping['review']}?per_page=100');
      if (response.statusCode == 200) {
        for (Map<String, dynamic> item in convert.jsonDecode(response.body)) {
          try {
            var listingId = Tools.getValueByKey(
                item, DataMapping().kListingReviewMapping['item']);
            if (listingId.toString() == (productId.toString())) {
              list.add(Review.fromListing(item));
            }
          } catch (e) {
            printLog('Error converting review Listing $e');
          }
        }
      }
      return list;
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>> searchProducts(
      {name, categoryId, tag, attribute, attributeId, page, lang}) async {
    try {
      List<Product> list = [];

      var endPoint =
          "$url/wp-json/wp/v2/${DataMapping().kProductPath}?search=$name&page=$page&per_page=50";
      if (categoryId != null) {
        endPoint += '&{DataMapping().kCategoryPath}=$categoryId';
      }
      var response = await http.get(endPoint);

      for (var item in convert.jsonDecode(response.body)) {
        Product product = Product.fromListingJson(item);
        List<String> _gallery = [];
        for (var item in product.images) {
          if (!item.contains('http')) {
            var res = await http.get('$url/wp-json/wp/v2/media/$item');

            _gallery.add(convert.jsonDecode(res.body)['source_url']);
          } else {
            _gallery.add(item);
          }
        }
        product.images = _gallery;
        list.add(product);
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Null> createReview(
      {String productId, Map<String, dynamic> data}) async {
    try {
      if (serverConfig['type'] == 'listpro') {
        await http.post('$url/wp-json/wp/v2/submitReview', body: {
          'listing_id': productId.toString(),
          'post_content': data['post_content'],
          'post_author': data['post_author'].toString(),
          'post_title': data['post_title'],
          'rating': data['rating'].toString(),
        });
      }
      if (serverConfig['type'] == 'listeo') {
        var request = http.MultipartRequest(
            'POST', Uri.parse('$url/wp-comments-post.php'));
        request.fields['comment_post_ID'] = productId.toString();
        request.fields['comment'] = data['post_content'];
        request.fields['submit'] = 'Post Comment';
        request.fields['comment_parent'] = '0';
        request.fields['value-for-money'] = data['rating'].toString();
        request.fields['service'] = data['rating'].toString();
        request.fields['location'] = data['rating'].toString();
        request.fields['cleanliness'] = data['rating'].toString();
        request.fields['email'] = data['email'].toString();
        request.fields['author'] = data['name'].toString();
        await request.send();
      }
      if (serverConfig['type'] == 'mylisting') {
        var request = http.MultipartRequest(
            'POST', Uri.parse('$url/wp-comments-post.php'));
        request.fields['comment_post_ID'] = productId.toString();
        request.fields['comment'] = data['post_content'];
        request.fields['submit'] = 'Post Comment';
        request.fields['comment_parent'] = '0';
        request.fields['rating_star_rating'] = (data['rating'] * 2).toString();
        request.fields['hospitality_star_rating'] =
            (data['rating'] * 2).toString();
        request.fields['service_star_rating'] = (data['rating'] * 2).toString();
        request.fields['pricing_star_rating'] = (data['rating'] * 2).toString();
        request.fields['email'] = data['email'].toString();
        request.fields['author'] = data['name'].toString();
        await request.send();
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<BookStatus> bookService({userId, value, message}) async {
    try {
      var str = convert.jsonEncode(value);
      var response = await http.post('$url/wp-json/wp/v2/booking', body: {
        'user_id': userId.toString(),
        'value': str,
        'message': message,
      });
      String status = convert.jsonDecode(response.body);
      BookStatus bookStatus;
      switch (status) {
        case 'booked':
          {
            bookStatus = BookStatus.booked;
            break;
          }

        case 'waiting':
          {
            bookStatus = BookStatus.waiting;
            break;
          }

        case 'confirmed':
          {
            bookStatus = BookStatus.confirmed;
            break;
          }

        case 'unavailable':
          {
            bookStatus = BookStatus.unavailable;
            break;
          }

        default:
          {
            bookStatus = BookStatus.error;
            break;
          }
      }
      return bookStatus;
    } catch (e) {
      printLog('bookService error: $e');
      return BookStatus.error;
    }
  }

  @override
  Future<List<Product>> getProductNearest(location) async {
    try {
      List<Product> list = [];
      var lat = location.latitude;
      var long = location.longitude;
      String urlReq =
          '$url/wp-json/wp/v2/${DataMapping().kProductPath}?status=publish&_embed=true';
      if (lat != 0 || long != 0) {
        urlReq += '&isGetLocate=true&lat=$lat&long=$long';
      }
      final response = await http.get(urlReq);
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)) {
          Product product = Product.fromListingJson(item);
          List<String> _gallery = [];
          for (var item in product.images) {
            if (!item.contains('http')) {
              var res = await http.get('$url/wp-json/wp/v2/media/$item');
              _gallery.add(convert.jsonDecode(res.body)['source_url']);
            } else {
              _gallery.add(item);
            }
          }
          product.images = _gallery;
          list.add(product);
        }
      }
      return list;
    } catch (err) {
      printLog('err at getProductRecents func ${err.toString()}');
      rethrow;
    }
  }

  @override
  Future<List<ListingBooking>> getBooking({userId, page, perPage}) async {
    var endpoint =
        '$url//wp-json/wp/v2/get-bookings?user_id=$userId&page=$page&per_page=$perPage';
    List<ListingBooking> bookings = [];
    try {
      final response = await http.get(endpoint);
      for (var item in convert.jsonDecode(response.body)) {
        var booking = ListingBooking.fromJson(item);
        bookings.add(booking);
      }
    } catch (e) {
      printLog('listing.dart getBooking $e');
    }
    return bookings;
  }

  @override
  Future<Map<String, dynamic>> checkBookingAvailability({data}) async {
    var endpoint = '$url/wp-json/wp/v2/check-availability';
    try {
      final response =
          await http.post(endpoint, body: convert.jsonEncode(data), headers: {
        'Content-type': 'application/json',
        "Accept": "application/json",
      });

      return convert.jsonDecode(response.body);
    } catch (e) {
      printLog('listing.dart checkBookingAvailability $e');
    }
    return {};
  }
}
