import '../../../services/service_config.dart';
import '../index.dart';
import 'listing.dart';

mixin ListingMixin on ConfigMixin {
  configListing(appConfig) {
    ListingService().appConfig(appConfig);
    serviceApi = ListingService();
    widget = ListingWidget();
  }
}
